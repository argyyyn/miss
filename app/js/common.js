$(function() {

	var galleryTop = new Swiper('.gallery-top', {
		spaceBetween: 10,
		navigation: {
			nextEl: '.right-2',
			prevEl: '.left-2',
		},
		loop: true,
		loopedSlides: 5,
		autoHeight: true
	});
	var galleryThumbs = new Swiper('.gallery-thumbs', {
		spaceBetween: 10,
		centeredSlides: true,
		slidesPerView: 'auto',
		touchRatio: 0.2,
		slideToClickedSlide: true,
		loop: true,
		loopedSlides: 5
	});
	galleryTop.controller.control = galleryThumbs;
	galleryThumbs.controller.control = galleryTop;


	$('#phone-id').mask('+7(999) 999-99-99',{placeholder: "+7 (   )   -  -  "});

	//E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			$(location).attr('href', '/thanks.html');
		});
		return false;
	});

});
